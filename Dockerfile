FROM amazon/aws-cli:2.22.3

RUN yum install -y tar gzip

# GitLab's AutoDevOps does not have multiarch support atm. See https://gitlab.com/gitlab-org/gitlab/-/issues/214552
ENV TARGETARCH=amd64

WORKDIR /usr/local/bin/

# Install kubectl
# Get latest available Version with: curl -Ls https://dl.k8s.io/release/stable.txt
# See https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
# renovate: datasource=github-releases depName=kubernetes/kubernetes extractVersion=^v(?<version>.*)$
ENV KUBECTL_VERSION=1.31.3
# hadolint ignore=SC3009
# hadolint ignore=SC2059
RUN curl --location --fail --remote-name "https://dl.k8s.io/release/v${KUBECTL_VERSION}/bin/linux/${TARGETARCH}/kubectl{,.sha256}" && \
    printf "$(cat kubectl.sha256)  kubectl\n" | sha256sum -c  && \
    chmod +x /usr/local/bin/kubectl

# Install kustomize
# For releases see here https://github.com/kubernetes-sigs/kustomize/releases
# TODO: renovate does not WORK with this repo
# renovate: datasource=github-releases lookupName=kubernetes-sigs/kustomize
ENV KUSTOMIZE_VERSION=5.5.0

RUN curl --location --fail https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv${KUSTOMIZE_VERSION}/kustomize_v${KUSTOMIZE_VERSION}_linux_${TARGETARCH}.tar.gz -o /tmp/kustomize_${KUSTOMIZE_VERSION}_linux_${TARGETARCH}.tar.gz && \
    tar -zxf /tmp/kustomize_${KUSTOMIZE_VERSION}_linux_${TARGETARCH}.tar.gz -C /usr/local/bin/ && \
    rm /tmp/kustomize_${KUSTOMIZE_VERSION}_linux_${TARGETARCH}.tar.gz
