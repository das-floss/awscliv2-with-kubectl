# awscli v2 with kubectl and kustomize

GitLab AutoDevOps builds images and makes them available in the public GitLab container registry.

Use it with `registry.gitlab.com/das-floss/awscliv2-with-kubectl:latest` or see releases the page to image tags.

## Verification of signed Image

    cosign verify registry.gitlab.com/das-floss/awscliv2-with-kubectl:latest --certificate-identity "https://gitlab.com/das-floss/awscliv2-with-kubectl//.gitlab-ci.yml@refs/heads/main" --certificate-oidc-issuer "https://gitlab.com"

## Links

* [Use Sigstore for keyless signing and verification](https://docs.gitlab.com/ee/ci/yaml/signing_examples.html)
